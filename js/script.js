$(document).ready(function () {
  //change menu bars to cross
  $('.navbar-toggler').click(function () {
    $('.navbar-toggler').toggleClass('change');
  });

  //sticky navbar and less padding
  $(window).scroll(function () {
    let position = $(this).scrollTop();

    //if navbar reaches the top ==> 2 css classes added
    if (position >= 718) {
      $('.navbar').addClass('navbar-background');
      $('.navbar').addClass('fixed-top');
    } else {
      //if position is < 718
      $('.navbar').removeClass('navbar-background');
      $('.navbar').removeClass('fixed-top');
    }
  });

  // Add smooth scrolling to all links
  $('a').on('click', function (event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== '') {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate(
        {
          scrollTop: $(hash).offset().top - 20,
        },
        2000,
        function () {
          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        }
      );
    } // End if
  });

  //ripples
  $('#header, .info').ripples({
    dropRadius: 25,
    perturbance: 0.012,
    resolution: 512,
  });

  //magnific popup
  $('.parent-container').magnificPopup({
    delegate: 'a', // child items selector, by clicking on it popup will open
    type: 'image',
    gallery: {
      enabled: true,
    },
    // other options
  });
});
