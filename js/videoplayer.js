$('.play-single').magnificPopup({
  type: 'iframe',
});

var items = [
  {
      //local video
    src: '../video/video-01.mp4',
  },
  //   {
  //     src: 'https://vimeo.com/80736183',
  //   },
  //   {
  //     src: 'https://vimeo.com/80101893',
  //   },
];

$('.play-all').each(function () {
  $(this).magnificPopup({
    type: 'iframe',
    items: items,
    gallery: {
      enabled: true,
    },
    // override the default vimeo service to activate the player api
    iframe: {
      patterns: {
        // vimeo: {
        //   index: 'vimeo.com/',
        //   id: '/',
        //   src:
        //     '//player.vimeo.com/video/%id%?autoplay=1&api=1&player_id=playall',
        // },
        // youtube: {
        //   index: 'youtube.com/',
        //   id: 'v=SeHT0ee4waU',
        //   src:
        //     '//www.youtube.com/embed/%id%?autoplay=1&api=1&player_id=playall',
        // },
      },
    },
    callbacks: {
      open: function () {
        var iframe = $(this.content).find('iframe').attr('id', 'playall')[0],
          player = $f(iframe),
          magnificInstance = this;

        // set up the handlers
        player.addEvent('ready', function (id) {
          player.addEvent('finish', function () {
            magnificInstance.next();
          });
        });
      },
    },
  });
});

/*#################################################################################################################*/

